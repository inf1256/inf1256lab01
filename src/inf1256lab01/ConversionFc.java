/*
 * Johnny Tsheke -- INF1256@UQAM
 * ce premier lab ne tient pas compte des exceptions 
 */
package inf1256lab01;
import java.util.*;//importation des classes
public class ConversionFc {

	public static void main(String[] args) {
         Scanner  scanne=new Scanner(System.in);
         double degreFahrenheit;
         double degreCelcius;
         System.out.println("Entrez la temperature en dégré Fahrenheit svp");
         degreFahrenheit=scanne.nextDouble();
         degreCelcius=(degreFahrenheit-32)*(5/9.0);
         System.out.println("La temperature en degre Celcius= "+ degreCelcius);
         scanne.close();
	}

}
