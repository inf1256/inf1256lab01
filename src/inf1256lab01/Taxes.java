/* Johnny Tsheke -- INF1256@UQAM
 * ce premier lab ne tient pas compte des exceptions 
 */
package inf1256lab01;
import java.util.*;
public class Taxes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner  scanne=new Scanner(System.in);
		double montantHorsTaxes;
		double  montantTPS;
		double montantTVQ;
		double montantTotalAvecTaxes;
		System.out.println("Entrez le montant total avant taxes svp");
		montantHorsTaxes=scanne.nextDouble();
		montantTPS=montantHorsTaxes*0.05;
		montantTVQ=montantHorsTaxes*0.09975;
		montantTotalAvecTaxes=montantHorsTaxes+montantTPS+montantTVQ;
		System.out.println("Montant hors taxes = "+montantHorsTaxes);
		System.out.println("Montant TPS = "+montantTPS);
		System.out.println("Montant TVQ = "+montantTVQ);
		System.out.println("Montant total avec taxes = "+montantTotalAvecTaxes);
		
		scanne.close();
	}

}
